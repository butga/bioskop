import React, { useState } from 'react';
import CurrencyFormat from 'react-currency-format';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, Input, Row, Col } from 'reactstrap';

const DetailBooking = (props) => {

    const [modal, setModal] = useState(false);

    const toggle = () => setModal(!modal);

    return (
        <>
            <Button
                color="warning"
                size="sm"
                className="text-white"
                onClick={toggle}>
                Detail
            </Button>
            <Modal isOpen={modal} toggle={toggle}>
                <ModalHeader toggle={toggle}>Detail Order</ModalHeader>
                <ModalBody>
                    <Form>
                        <FormGroup>
                            <Label for="name" className="mb-2">Movie Name</Label>
                            <Input type="text" name="name" id="name" className="mb-2"
                                value={props.order.name}
                                readOnly />
                        </FormGroup>
                        <Row>
                            <Col>
                                <FormGroup>
                                    <Label for="room" className="mb-2">Room</Label>
                                    <Input type="text" name="room" id="room" className="mb-2"
                                        value={props.order.roomName}
                                        readOnly />
                                </FormGroup>
                            </Col>
                            <Col>
                                <FormGroup>
                                    <Label for="showTime" className="mb-2">Time</Label>
                                    <Input type="text" name="showTime" id="showTime" className="mb-2"
                                        value={props.order.showTime}
                                        readOnly />
                                </FormGroup>
                            </Col>
                            <Col>
                                <FormGroup>
                                    <Label for="showDate" className="mb-2">Date</Label>
                                    <Input type="text" name="showDate" id="showDate" className="mb-2"
                                        value={props.order.showDate}
                                        readOnly />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <FormGroup>
                                    <Label for="totalTicket" className="mb-2">Total Ticket</Label>
                                    <Input type="number" name="totalTicket" id="totalTicket" className="mb-2"
                                        value={props.order.totalTicket}
                                        readOnly />
                                </FormGroup>
                            </Col>
                            <Col>
                                <FormGroup>
                                    <Label for="totalPrice" className="mb-2">Total Price</Label>
                                    <CurrencyFormat
                                        value={props.order.totalPrice}
                                        displayType={"text"}
                                        thousandSeparator={true}
                                        prefix={"Rp. "}
                                        renderText={(values) => (
                                            <Input name="totalPrice" id="totalPrice" className="mb-2"
                                                value={values}
                                                readOnly />
                                        )}
                                    />
                                </FormGroup>
                            </Col>
                        </Row>
                        <FormGroup>
                            <Label for="orderStatus" className="mb-2">Order Status</Label>
                            <Input type="text" name="orderStatus" id="orderStatus" className="mb-2"
                                value={props.order.orderStatus}
                                readOnly />
                        </FormGroup>
                    </Form>
                </ModalBody>
                <ModalFooter>
                    <Button color="secondary" onClick={toggle}>Back</Button>
                </ModalFooter>
            </Modal>
        </>
    );
}

export default DetailBooking;