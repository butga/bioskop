import jwtDecode from 'jwt-decode';
import React from 'react';
import swal from 'sweetalert';
import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';
import { Route, Switch, useRouteMatch } from 'react-router';
import { authAction } from '../../../redux/reducer/AuthReducer';
import ContentAcountMember from './ContentAccountMember';
import ContentHistoryMember from './ContentHistoryMember';
import ContentHomeMember from '../content/ContentHomeMember';
import ContentMoviesMember from '../content/ContentMoviesMember';
import MovieRegDetail from './MovieRegDetail';
import MovieVipDetail from './MovieVipDetail';
import SeatRegOrder from './SeatRegOrder';
import SeatVipOrder from './SeatVipOrder';

const ContentMember = () => {

    const token = useSelector((state) => state.auth.token);

    const dispatch = useDispatch();

    let {path} = useRouteMatch();

    if (token) {
        const decoded = jwtDecode(token);
        if (decoded.exp < Date.now() / 1000) {
            swal("Error!", "Your session expired", "error");
            dispatch(authAction.logout());
        }
    }

    return (
        <Switch>
            <Route exact path={`${path}`} component={ContentHomeMember} />
            <Route exact path={`${path}/movies`} component={ContentMoviesMember} />
            <Route exact path={`${path}/movies-vip/:id`} component={MovieVipDetail} />
            <Route exact path={`${path}/movies-regular/:id`} component={MovieRegDetail} />
            <Route exact path={`${path}/account`} component={ContentAcountMember} />
            <Route exact path={`${path}/booking-history`} component={ContentHistoryMember} />
            <Route exact path={`${path}/seat-regular/:id`} component={SeatRegOrder} />
            <Route exact path={`${path}/seat-vip/:id`} component={SeatVipOrder} />
        </Switch>
    );
}

export default ContentMember;