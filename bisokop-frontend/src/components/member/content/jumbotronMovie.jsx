import React from 'react';
import CurrencyFormat from 'react-currency-format';
import { Jumbotron, Row, Col, Table, Card } from 'reactstrap';
import '../../../assets/css/jumbotronMovie.css'

const JumbotronMovie = (props) => {
    return (
        <div>
            <Jumbotron>
                <Row>
                    <Col sm="4">
                        <Card className="img-detail-movie">
                            <img
                                src={`data:${props.movie.dataImage};base64, ` + props.movie.dataImage}
                                className="image-detail-movie" alt="..." />
                        </Card>
                    </Col>
                    <Col sm="8">
                        <h1 className="display-3">{props.movie.name}</h1>
                        <p className="lead">{props.movie.description.substring(0,500)} ...</p>
                        <hr className="my-2" />
                        <Table>
                            <tbody>
                                <tr>
                                    <td>Room</td>
                                    <td>{props.movie.roomName}</td>
                                </tr>
                                <tr>
                                    <td>Price</td>
                                    <CurrencyFormat
                                        value={props.movie.price}
                                        displayType={"text"}
                                        thousandSeparator={true}
                                        prefix={"Rp. "}
                                        renderText={(value) => (
                                            <td>{value}</td>
                                        )}
                                    />
                                </tr>
                                <tr>
                                    <td>Show Time</td>
                                    <td>{props.movie.showTime}</td>
                                </tr>
                                <tr>
                                    <td>Show Date</td>
                                    <td>{props.movie.showDate}</td>
                                </tr>
                            </tbody>
                        </Table>
                    </Col>
                </Row>
            </Jumbotron>
        </div>
    );
};

export default JumbotronMovie;