import React, { useState } from 'react';
import '../../assets/css/navbarAdmin.css'
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Container
} from 'reactstrap';
import { Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { authAction } from '../../redux/reducer/AuthReducer';

const NavbarAdmin = (props) => {
    const [isOpen, setIsOpen] = useState(false);

    const toggle = () => setIsOpen(!isOpen);

    const dispatch = useDispatch()

    return (
        <div>
            <Navbar className="navbar-default" light expand="md">
                <Container>
                    <NavbarBrand>
                        NEXMOVI
                    </NavbarBrand>
                    <NavbarToggler onClick={toggle} />
                    <Collapse isOpen={isOpen} navbar className="navbar-collapse-admin">
                        <Nav navbar>
                            <Link to="/admin" className="navbar navbar-items">
                                Dashboard
                            </Link>
                            <UncontrolledDropdown className="navbar navbar-items">
                                <DropdownToggle nav caret>
                                    Schedule
                                </DropdownToggle>
                                <DropdownMenu right>
                                    <DropdownItem>
                                        <Link to="/admin/room" className="navbar navbar-items">
                                            <span className="bi bi-door-open"> Rooms</span>
                                        </Link>
                                    </DropdownItem>
                                    <DropdownItem>
                                        <Link to="/admin/movie" className="navbar navbar-items">
                                            <span className="bi bi-film"> Movies</span>
                                        </Link>
                                    </DropdownItem>
                                    <DropdownItem>
                                        <Link to="/admin/schedule" className="navbar navbar-items">
                                            <span className="bi bi-calendar-week"> Schedule</span>
                                        </Link>
                                    </DropdownItem>
                                </DropdownMenu>
                            </UncontrolledDropdown>
                            <Link to="/admin/user" className="navbar navbar-items">
                                User
                            </Link>
                            <UncontrolledDropdown className="navbar navbar-items">
                                <DropdownToggle nav caret>
                                    Orders
                                </DropdownToggle>
                                <DropdownMenu right>
                                    <DropdownItem>
                                        <Link to="/admin/order-request" className="navbar navbar-items">
                                            <span className="bi bi-journal-text"> Order Request</span>
                                        </Link>
                                    </DropdownItem>
                                    <DropdownItem>
                                        <Link to="/admin/order" className="navbar navbar-items">
                                            <span className="bi bi-journal-check"> Order Report</span>
                                        </Link>
                                    </DropdownItem>
                                </DropdownMenu>
                            </UncontrolledDropdown>
                        </Nav>
                        <Nav navbar>
                            <UncontrolledDropdown className="navbar navbar-items">
                                <DropdownToggle nav caret>
                                    Account
                                </DropdownToggle>
                                <DropdownMenu right>
                                    <DropdownItem>
                                        <Link to="/admin/account" className="navbar navbar-items">
                                            <span className="bi bi-person"> Account</span>
                                        </Link>
                                    </DropdownItem>
                                    <DropdownItem>
                                        <a
                                            href="#!"
                                            className="navbar navbar-items"
                                            onClick={() => dispatch(authAction.logout())}>
                                            <span className="bi bi-box-arrow-left"> Logout</span>
                                        </a>
                                    </DropdownItem>
                                </DropdownMenu>
                            </UncontrolledDropdown>
                        </Nav>
                    </Collapse>
                </Container>
            </Navbar>
        </div>
    );
}

export default NavbarAdmin;