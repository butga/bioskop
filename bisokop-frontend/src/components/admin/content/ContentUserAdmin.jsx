import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { getData } from '../../../service/Fetch';
import FormAddAdmin from '../form/FormAddAdmin';
import FormEditMember from '../form/FormEditMember';
import ModalDetailUser from '../form/ModalDetailUser';
import '../../../assets/css/contentUserAdmin.css'
import {
    Button,
    Col,
    Container,
    Row,
    Table
} from 'reactstrap';

const ContentUserAdmin = () => {

    const [users, setUsers] = useState([]);

    const userActive = useSelector((state) => state.auth.user);

    const token = useSelector(state => state.auth.token);

    const [response, setResponse] = useState();

    const [paginated, setPaginated] = useState({
        empty: true,
        first: true,
        last: true,
        totalPages: 0,
    });

    const [keyword, setKeyword] = useState("");

    const [finalKeyword, setFinalKeyword] = useState("");

    const [page, setPage] = useState(0);

    useEffect(() => {
        const url = `admin/users-paginated?pageNo=${page}&keyword=${finalKeyword}`

        getData(url, token)
            .then(res => {
                setUsers(res.data.content);
                setPaginated({
                    empty: res.data.empty,
                    first: res.data.first,
                    last: res.data.last,
                    totalPages: res.data.totalPages
                })
            })
    }, [response, page, finalKeyword])

    const searchHandler = () => {
        setFinalKeyword(keyword);
    }

    const resetHandler = () => {
        setFinalKeyword("");
        setKeyword("");
    }

    return (
        <Container className="pt-2">
            <Row className="mt-4 mb-4">
                <Col>
                    <h5>User Management</h5>
                </Col>
                <Col className="flex-column align-items-end d-none d-md-flex">
                    <nav aria-label="breadcrumb">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item"><Link to="/admin"><i class="bi bi-house"></i></Link></li>
                            <li className="breadcrumb-item active" aria-current="page">User</li>
                        </ol>
                    </nav>
                </Col>
            </Row>
            <Row className="mt-4 mb-4">
                <Col>
                    <FormAddAdmin setResponse={setResponse} />
                </Col>
                <Col className="d-flex justify-content-end">
                    <div className="col-lg-6 d-flex">
                        <input
                            type="text"
                            className="form-control input-search"
                            value={keyword}
                            placeholder="Search by username ..."
                            name="key"
                            onChange={(e) => setKeyword(e.target.value)}
                        />
                        {
                            keyword !== "" &&
                            <button
                                className="btn btn-sm btn-danger btn-search"
                                onClick={resetHandler}
                            >
                                <span><i class="bi bi-x-square"></i></span>
                            </button>
                        }
                        <button
                            className="btn btn-sm btn-primary btn-search"
                            onClick={searchHandler}
                        >
                            <i>Search</i>
                        </button>
                    </div>
                </Col>
            </Row>
            <Row className="mt-4 mb-4">
                <Col>
                    <Table striped bordered hover className="text-center">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                users.length !== 0 ?
                                    users.map(user =>
                                        <tr key={user.id}>
                                            <td>#</td>
                                            <td>{user.username}</td>
                                            <td>{user.email}</td>
                                            {
                                                // cek role member
                                                user.role === "ROLE_ADMIN" ?
                                                    <td>ADMIN</td> : <td>MEMBER</td>
                                            }
                                            {
                                                // hak akses untuk edit
                                                user.role === 'ROLE_ADMIN' ?
                                                    userActive.id === user.id ?
                                                        <td>
                                                            <div className="d-flex justify-content-center">
                                                                <Link to="account">
                                                                    <Button className="btn-detail-user" color="warning" size="sm"><i class="bi bi-pencil-square"></i></Button>
                                                                </Link>
                                                                <ModalDetailUser user={user} />
                                                            </div>
                                                        </td>
                                                        :
                                                        <td>
                                                            <div className="d-flex justify-content-center">
                                                                <Button className="btn-detail-user" color="warning" size="sm" disabled><i class="bi bi-pencil-square"></i></Button>
                                                                <ModalDetailUser user={user} />
                                                            </div>
                                                        </td>
                                                    :
                                                    <td>
                                                        <div className="d-flex justify-content-center">
                                                            <FormEditMember user={user} setResponse={setResponse} />
                                                            <ModalDetailUser user={user} />
                                                        </div>
                                                    </td>
                                            }
                                        </tr>
                                    ) :
                                    <tr>
                                        <td colSpan="5" className="text-center">Data Kosong</td>
                                    </tr>
                            }
                        </tbody>
                    </Table>
                </Col>
            </Row>

            {/* Pagination */}
            <Row>
                <Col className="d-flex justify-content-center">
                    <nav>
                        {
                            paginated.empty ?
                                <p></p> :
                                <ul className="pagination">
                                    {
                                        paginated.first ?
                                            <li className="page-item disabled">
                                                <span className="page-link">Previous</span>
                                            </li> :
                                            <li
                                                onClick={() => setPage(page - 1)}
                                                className="page-item">
                                                <span className="page-link">Previous</span>
                                            </li>
                                    }
                                    {
                                        Array.from(Array(paginated.totalPages), (event, index) => {
                                            return (
                                                <li
                                                    onClick={() => setPage(index)}
                                                    className={index === page ? "page-item active" : "page-item"}>
                                                    <span className="page-link">
                                                        {index + 1}
                                                    </span>
                                                </li>
                                            );
                                        })
                                    }
                                    {
                                        paginated.last ?
                                            <li className="page-item disabled">
                                                <span className="page-link">Next</span>
                                            </li> :
                                            <li
                                                onClick={() => setPage(page + 1)}
                                                className="page-item">
                                                <span className="page-link">Next</span>
                                            </li>
                                    }
                                </ul>
                        }
                    </nav>
                </Col>
            </Row>
        </Container>
    );
}

export default ContentUserAdmin;