import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Link, useLocation } from 'react-router-dom';
import { Col, Container, Row, Button } from 'reactstrap';
import { getData } from '../../../service/Fetch';
import '../../../assets/css/contentSeat.css';

const ContentSeatVip = () => {

    const schedule = useLocation();

    const s = schedule.state.data;

    const token = useSelector(state => state.auth.token);

    const [seats, setSeats] = useState([]);

    async function fetchTicket() {
        let response = await getData(`ticket/${s.id}`, token)
        return response.data
    }

    async function setupSeat() {
        let listSeats = []

        for (let i = 1; i <= 6; i++) {
            let seatLoop = {
                id: "U" + i,
                name: "U" + i,
                status: "unreserved"
            }

            listSeats.push(seatLoop);
        }

        for (let i = 1; i <= 6; i++) {
            let seatLoop = {
                id: "D" + i,
                name: "D" + i,
                status: "unreserved"
            }

            listSeats.push(seatLoop);
        }

        return listSeats;
    }


    useEffect(() => {

        async function combineSeat() {
            let reservedSeats = await fetchTicket();
            let staticSeats = await setupSeat();

            staticSeats.map(staticSeat =>
                reservedSeats.map((reservedSeat) => {
                    if (staticSeat.id === reservedSeat.seat.id) {
                        staticSeat.status = reservedSeat.status
                    }
                })
            )

            setSeats(staticSeats);
        }

        combineSeat()

    }, [])



    return (
        <Container className="pt-2">
            <Row className="mt-4 mb-4">
                <Col>
                    <h5>Seat {s.room.name}</h5>
                </Col>
                <Col className="flex-column align-items-end d-none d-md-flex">
                    <nav aria-label="breadcrumb">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item"><Link to="/admin"><i class="bi bi-house"></i></Link></li>
                            <li className="breadcrumb-item"><Link to="/admin/schedule" className="breadcrumb-item-link">List Schedule</Link></li>
                            <li className="breadcrumb-item active" aria-current="page">Seats</li>
                        </ol>
                    </nav>
                </Col>
            </Row>
            <Row>
                <Col>
                    <p>{s.movie.name}</p>
                </Col>
                <Col >
                    <p className="d-flex justify-content-end">{s.showTime} | {s.showDate}</p>
                </Col>
            </Row>
            <Row className="d-flex justify-content-center mt-5 mb-5">
                <Col sm="8">
                    <span className=" d-block text-center p-2 bg-secondary text-white">Layar</span>
                </Col>
            </Row>
            <Row>
                {
                    seats.slice(0, 6).map(seat =>
                        seat.status === "unreserved" ?
                            <Col className="m-5">
                                <Button size="lg" className="btn-seat-vip" color="secondary" disabled>{seat.name}</Button>
                            </Col> :
                            <Col className="m-5">
                                <Button size="lg" className="btn-seat-vip" color="danger" disabled>{seat.name}</Button>
                            </Col>
                    )
                }
            </Row>
            <Row>
                {
                    seats.slice(6, 12).map(seat =>
                        seat.status === "unreserved" ?
                            <Col className="m-5">
                                <Button size="lg" className="btn-seat-vip" color="secondary" disabled>{seat.name}</Button>
                            </Col> :
                            <Col className="m-5">
                                <Button size="lg" className="btn-seat-vip" color="danger" disabled>{seat.name}</Button>
                            </Col>
                    )
                }
            </Row>
            <Row>
                <Col><h6>Informasi</h6></Col>
            </Row>
            <Row className="mb-2 mt-2">
                <Col sm="1" className="text-center"><Button color="secondary" disabled>U1</Button></Col>
                <Col sm="11"><p>unreserved</p></Col>
            </Row>
            <Row className="mb-2 mt-2">
                <Col className="text-center" sm="1"><Button color="danger" disabled>U2</Button></Col>
                <Col sm="11"><p>reserved</p></Col>
            </Row>
            <Row>
                <Col className="d-flex justify-content-center mt-5 mb-5">
                    <Link to="/admin/schedule">
                        <Button color="secondary"> <span className="bi bi-arrow-left-circle"> Back</span></Button>
                    </Link>
                </Col>
            </Row>
        </Container>
    );
}

export default ContentSeatVip;