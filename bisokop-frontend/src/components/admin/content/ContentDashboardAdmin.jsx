import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Col, Container, Row } from 'reactstrap';
import '../../../assets/css/contentDashboardAdmin.css'
import { getData } from '../../../service/Fetch';
import CardDashboard from '../card/CardDashboard';

const ContentDashboardAdmin = () => {

    const username = useSelector((state) => state.auth.user.username);

    const [data, setData] = useState()

    const token = useSelector(state => state.auth.token);

    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        getData(`admin/dashboard`, token)
            .then(res => {
                setData(res.data);
                setIsLoading(false)
            })
    }, [])

    return (
        <>
            {
                isLoading ?
                    <Container>
                        <Row>
                            <p>Loading data ...</p>
                        </Row>
                    </Container> :
                    <>
                        <Container className="pt-3 mt-3">
                            <Row>
                                <Col>
                                    <h5 className="title">Welcome back, {username}!</h5>
                                </Col>
                            </Row>
                        </Container>
                        <Container className="mt-3">
                            <Row>
                                <Col sm="6" lg="4">
                                    <CardDashboard
                                        card="card-box bg-blue"
                                        amount={data.movies}
                                        title="Movies showing today"
                                        link="/admin/schedule"
                                        type="text"
                                    />
                                </Col>
                                <Col sm="6" lg="4">
                                    <CardDashboard
                                        card="card-box bg-orange"
                                        amount={data.tickets}
                                        title="Tickets sold out today"
                                        link="/admin/order"
                                        type="text"
                                    />
                                </Col>
                                <Col sm="6" lg="4">
                                    <CardDashboard
                                        card="card-box bg-green"
                                        amount={data.incomes}
                                        title="Total income today"
                                        link="/admin/order"
                                        type="currency"
                                    />
                                </Col>
                            </Row>
                        </Container>
                    </>
            }
        </>
    );
}

export default ContentDashboardAdmin;