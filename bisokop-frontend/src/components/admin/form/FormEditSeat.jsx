import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { Button, Modal, ModalHeader, ModalBody, Form, FormGroup, Label, Input } from 'reactstrap';
import swal from 'sweetalert';
import { postData } from '../../../service/Fetch';

const FormEditSeat = (props) => {

    const [modal, setModal] = useState(false);

    const token = useSelector(state => state.auth.token);

    const [newSeat, setNewSeat] = useState({
        id: props.seat.id,
        name: props.seat.name
    });

    const toggle = () => {
        setNewSeat({
            id: props.seat.id,
            name: props.seat.name
        });
        setModal(!modal);
    };

    const submitHandler = (event) => {

        event.preventDefault();

        postData(`admin/seat/${newSeat.id}`, newSeat, token)
            .then(res => {
                props.setSeat(res.data)
            })
            .catch(err => {
                swal("Error", err.response.data, "error");
            });

        toggle();
    }

    return (
        <div>
            <Button color="warning" className="text-white" onClick={toggle} size="sm">Edit</Button>
            <Modal isOpen={modal} toggle={toggle}>
                <ModalHeader toggle={toggle}>Edit Seat</ModalHeader>
                <ModalBody>
                    <Form onSubmit={submitHandler}>
                        <FormGroup>
                            <Label for="seat" className="mb-2">Seat Name</Label>
                            <Input
                                type="text" name="seat" id="seat" className="mb-2"
                                value={newSeat.name}
                                onChange={(e) => setNewSeat({ ...newSeat, name: e.target.value })}
                                required />
                        </FormGroup>
                        <div className="d-flex justify-content-end mt-3">
                            <Input
                                type="submit"
                                className="m-2 btn btn-primary"
                                value="Submit"
                                required>
                            </Input>
                            <Button color="danger" className="m-2" onClick={toggle}>Cancel</Button>{' '}
                        </div>
                    </Form>
                </ModalBody>
            </Modal>
        </div>
    );
}

export default FormEditSeat;