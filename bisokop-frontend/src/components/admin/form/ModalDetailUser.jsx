import React, { useState } from 'react';
import { 
    Button, 
    Modal, 
    ModalHeader, 
    ModalBody, 
    ModalFooter, 
    Form, 
    FormGroup, 
    Label, 
    Input, 
    Row, 
    Col 
} from 'reactstrap';

const ModalDetailUser = (props) => {

    const [modal, setModal] = useState(false);

    const toggle = () => setModal(!modal);

    return (
        <div>
            <Button color="primary" size="sm" className="btn-detail-user" onClick={toggle}><i class="bi bi-card-list"></i></Button>
            <Modal isOpen={modal} size="lg" toggle={toggle}>
                <ModalHeader toggle={toggle}>Detail User</ModalHeader>
                <ModalBody>
                    <Form>
                        <Row>
                            <Col>
                                <FormGroup>
                                    <Label for="name" className="mb-2">Name</Label>
                                    <Input type="text" name="name" id="name" className="mb-2"
                                        value={props.user.name}
                                        readOnly />
                                </FormGroup>
                            </Col>
                            <Col>
                                <FormGroup>
                                    <Label for="username" className="mb-2">Username</Label>
                                    <Input type="text" name="username" id="username" className="mb-2"
                                        value={props.user.username}
                                        readOnly />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <FormGroup>
                                    <Label for="email" className="mb-2">Email</Label>
                                    <Input type="text" name="email" id="email" className="mb-2"
                                        value={props.user.email}
                                        readOnly />
                                </FormGroup>
                            </Col>
                            <Col>
                                <FormGroup>
                                    <Label for="phone" className="mb-2">Phone Number</Label>
                                    <Input type="text" name="phone" id="phone" className="mb-2"
                                        value={props.user.phone_number}
                                        readOnly />
                                </FormGroup>
                            </Col>
                        </Row>
                        <FormGroup>
                            <Label for="address" className="mb-2">Address</Label>
                            <Input type="text" name="address" id="address" className="mb-2"
                                value={props.user.address}
                                readOnly />
                        </FormGroup>
                        <FormGroup>
                            <Label for="role" className="mb-2">Role</Label>
                            <Input type="text" name="role" id="role" className="mb-2"
                                value={props.user.role}
                                readOnly />
                        </FormGroup>
                        <FormGroup>
                            <Label for="created_at" className="mb-2">Registrasi</Label>
                            <Input type="text" name="created_at" id="created_at" className="mb-2"
                                value={props.user.created_at}
                                readOnly />
                        </FormGroup>
                    </Form>
                </ModalBody>
                <ModalFooter>
                    <Button color="secondary" onClick={toggle}>Back</Button>
                </ModalFooter>
            </Modal>
        </div>
    );
}

export default ModalDetailUser;