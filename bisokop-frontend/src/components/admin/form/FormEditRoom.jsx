import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import swal from 'sweetalert';
import {
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    Form,
    FormGroup,
    Label,
    Input,
    Row,
    Col
} from 'reactstrap';
import { postData } from '../../../service/Fetch';

const FormEditRoom = (props) => {
    const [modal, setModal] = useState(false);
    const token = useSelector(state => state.auth.token);
    const [newRoom, setNewRoom] = useState({
        id: props.room.id,
        name: props.room.name,
        type: props.room.type,
        price: props.room.price,
    });

    const toggle = () => {
        setModal(!modal);
    };

    const submitHandler = (event) => {

        event.preventDefault();

        postData(`admin/room/${newRoom.id}`, newRoom, token)
            .then(res => {
                props.setResponse(res.data);
                swal({
                    title: "Success!",
                    text: "Success Edit Room",
                    icon: "success",
                });
            })
            .catch(err => {
                swal("Error", err.response.data, "error");
            });

        toggle();
    }

    return (
        <div>
            <Button size="sm" className="text-white" color="warning" onClick={toggle}><i class="bi bi-pencil-square"></i></Button>
            <Modal isOpen={modal} toggle={toggle} className="modal-dialog">
                <ModalHeader toggle={toggle}>Edit Room</ModalHeader>
                <ModalBody>
                    <Form onSubmit={submitHandler}>
                        <FormGroup>
                            <Row>
                                <Col md="12">
                                    <Label for="name" className="mb-2">Room Name</Label>
                                    <Input type="text" name="name" id="name" className="mb-2"
                                        onChange={(e) => setNewRoom({ ...newRoom, name: e.target.value })}
                                        value={newRoom.name}
                                        required />
                                </Col>
                            </Row>
                        </FormGroup>
                        <FormGroup>
                            <Row>
                                <Col md="6">
                                    <Label for="type" className="mb-2">Type</Label>
                                    <Input type="select" name="type" id="type" className="mb-2"
                                        onChange={(e) => setNewRoom({ ...newRoom, type: e.target.value })}
                                        value={newRoom.type}
                                        disabled>
                                        <option value="REGULAR">REGULAR</option>
                                        <option value="VIP">VIP</option>
                                    </Input>
                                </Col>
                                <Col md="6">
                                    <Label for="price" className="mb-2">Price</Label>
                                    <Input type="number" name="price" id="price" min="1" className="mb-2"
                                        onChange={(e) => setNewRoom({ ...newRoom, price: e.target.value })}
                                        value={newRoom.price}
                                        required>
                                    </Input>
                                </Col>
                            </Row>
                        </FormGroup>
                        <div className="d-flex justify-content-end">
                            <input type="submit" className="btn btn-primary m-2" 
                                value="submit" />
                            <input type="button" className="btn btn-secondary m-2" 
                                value="Cancel" onClick={toggle} />
                        </div>
                    </Form>
                </ModalBody>
            </Modal>
        </div >
    );
}

export default FormEditRoom;