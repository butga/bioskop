import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import swal from 'sweetalert';
import { postData } from '../../../service/Fetch';
import {
    Button,
    Col,
    Form,
    FormGroup,
    Input,
    Label,
    Modal,
    ModalBody,
    ModalHeader,
    Row,
} from 'reactstrap';

const FormAddAdmin = (props) => {

    const [modal, setModal] = useState(false);

    const token = useSelector(state => state.auth.token);

    const [data, setData] = useState({
        name: '',
        username: '',
        password: '',
        address: '',
        phone_number: '',
        email: ''
    })

    const submitHandler = (event) => {
        event.preventDefault();

        postData("admin/register", data, token)
            .then(res => {
                props.setResponse(res.data);
                swal({
                    title: "Success!",
                    text: "Success Added Admin",
                    icon: "success",
                })
            })
            .catch(err => {
                swal("Error", err.response.data, "error");
            });

        toggle();
    }

    const toggle = () => {
        setModal(!modal)
    }


    return (<div>
        <Button
            color="success"
            onClick={toggle}
            size="sm">
            Add Admin
        </Button>
        <Modal isOpen={modal} toggle={toggle}>
            <ModalHeader
                toggle={toggle}>
                Add Admin
            </ModalHeader>
            <ModalBody>
                <Form onSubmit={submitHandler}>
                    <FormGroup>
                        <Label for="name" className="mb-2">Name</Label>
                        <Input
                            type="text"
                            name="name"
                            id="name"
                            className="mb-2"
                            onChange={(e) => setData({ ...data, name: e.target.value })}
                        />
                    </FormGroup>
                    <Row>
                        <Col>
                            <FormGroup>
                                <Label for="username" className="mb-2">Username</Label>
                                <Input
                                    type="text"
                                    name="username"
                                    id="username"
                                    className="mb-2"
                                    onChange={(e) => setData({ ...data, username: e.target.value })}
                                />
                            </FormGroup>
                        </Col>
                        <Col>
                            <FormGroup>
                                <Label for="number" className="mb-2">Phone Number</Label>
                                <Input
                                    type="text"
                                    name="number"
                                    id="number"
                                    className="mb-2"
                                    onChange={(e) => setData({ ...data, phone_number: e.target.value })}
                                />
                            </FormGroup>
                        </Col>
                    </Row>
                    <FormGroup>
                        <Label for="email" className="mb-2">Email</Label>
                        <Input
                            type="email"
                            name="email"
                            id="email"
                            className="mb-2"
                            onChange={(e) => setData({ ...data, email: e.target.value })}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="address" className="mb-2">Address</Label>
                        <Input
                            type="text"
                            name="address"
                            id="address"
                            className="mb-2"
                            onChange={(e) => setData({ ...data, address: e.target.value })}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="password" className="mb-2">Password</Label>
                        <Input
                            type="text"
                            name="password"
                            id="password"
                            className="mb-2"
                            onChange={(e) => setData({ ...data, password: e.target.value })}
                        />
                    </FormGroup>
                    <div className="d-flex justify-content-end mt-3">
                        <Input
                            type="submit"
                            className="m-2 btn btn-primary"
                            value="Submit"
                            required>
                        </Input>
                        <Button
                            color="danger"
                            className="m-2"
                            onClick={toggle}>
                            Cancel
                        </Button>
                    </div>
                </Form>
            </ModalBody>
        </Modal>
    </div>
    );
}

export default FormAddAdmin;