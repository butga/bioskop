import React, { useState } from 'react';
import CurrencyFormat from 'react-currency-format';
import {
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Form,
    FormGroup,
    Label,
    Input,
    Col,
    Row
} from 'reactstrap';

const ModalOrderDetail = (props) => {

    const [modal, setModal] = useState(false);

    const toggle = () => setModal(!modal);

    return (
        <>
            <Button
                onClick={toggle}
                className="btn-detail">
                <i class="bi bi-card-list"></i>
            </Button>
            <Modal isOpen={modal} toggle={toggle}>
                <ModalHeader toggle={toggle}>Order Detail Request</ModalHeader>
                <ModalBody>
                    <Form>
                        <FormGroup>
                            <Label for="name" className="mb-2">Member</Label>
                            <Input type="text" name="name" id="name" className="mb-2"
                                value={props.or.username}
                                readOnly />
                        </FormGroup>
                        <FormGroup>
                            <Label for="email" className="mb-2">Email</Label>
                            <Input type="text" name="email" id="email" className="mb-2"
                                value={props.or.email}
                                readOnly />
                        </FormGroup>
                        <FormGroup>
                            <Label for="movieName" className="mb-2">Movie</Label>
                            <Input type="text" name="movieName" id="movieName" className="mb-2"
                                value={props.or.name}
                                readOnly />
                        </FormGroup>
                        <Row>
                            <Col>
                                <FormGroup>
                                    <Label for="roomName" className="mb-2">Room</Label>
                                    <Input type="text" name="roomName" id="roomName" className="mb-2"
                                        value={props.or.roomName}
                                        readOnly />
                                </FormGroup>
                            </Col>
                            <Col>
                                <FormGroup>
                                    <Label for="showTime" className="mb-2">Show Time</Label>
                                    <Input type="text" name="showTime" id="showTime" className="mb-2"
                                        value={props.or.showTime}
                                        readOnly />
                                </FormGroup>
                            </Col>
                            <Col>
                                <FormGroup>
                                    <Label for="showDate" className="mb-2">Show Date</Label>
                                    <Input type="text" name="showDate" id="showDate" className="mb-2"
                                        value={props.or.showDate}
                                        readOnly />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <FormGroup>
                                    <Label for="totalTicket" className="mb-2">Total Ticket</Label>
                                    <Input type="text" name="totalTicket" id="totalTicket" className="mb-2"
                                        value={props.or.totalTicket}
                                        readOnly />
                                </FormGroup>
                            </Col>
                            <Col>
                                <FormGroup>
                                    <Label for="totalTicket" className="mb-2">Ticket Price</Label>
                                    <CurrencyFormat
                                        value={props.or.ticketPrice}
                                        displayType={"text"}
                                        thousandSeparator={true}
                                        prefix={"Rp. "}
                                        renderText={(values) => (
                                            <Input name="totalPrice" id="totalPrice" className="mb-2"
                                                value={values}
                                                readOnly />
                                        )}
                                    />
                                </FormGroup>
                            </Col>
                            <Col>
                                <FormGroup>
                                    <Label for="totalPrice" className="mb-2">Total Price</Label>
                                    <CurrencyFormat
                                        value={props.or.totalPrice}
                                        displayType={"text"}
                                        thousandSeparator={true}
                                        prefix={"Rp. "}
                                        renderText={(values) => (
                                            <Input name="totalPrice" id="totalPrice" className="mb-2"
                                                value={values}
                                                readOnly />
                                        )}
                                    />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <FormGroup>
                                    <Label for="orderDate" className="mb-2">Order Date</Label>
                                    <Input type="text" name="orderDate" id="orderDate" className="mb-2"
                                        value={props.or.orderDate}
                                        readOnly />
                                </FormGroup>
                            </Col>
                            <Col>
                                <FormGroup>
                                    <Label for="orderStatus" className="mb-2">Order Status</Label>
                                    <Input type="text" name="orderStatus" id="orderStatus" className="mb-2"
                                        value={props.or.orderStatus}
                                        readOnly />
                                </FormGroup>
                            </Col>
                        </Row>
                    </Form>
                </ModalBody>
                <ModalFooter>
                    <Button color="secondary" onClick={toggle}>Back</Button>
                </ModalFooter>
            </Modal>
        </>
    );
}

export default ModalOrderDetail;