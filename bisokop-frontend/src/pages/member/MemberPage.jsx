import React from 'react';
import ContentMember from '../../components/member/content/ContentMember';
import NavbarMember from '../../components/member/NavbarMember';

const MemberPage = () => {
    return (
        <>
            <NavbarMember />
            <ContentMember />
        </>
    );
}
 
export default MemberPage;