package id.co.nexsoft.bioskopbackend.repositories;

import id.co.nexsoft.bioskopbackend.dto.requests.TicketRequest;
import id.co.nexsoft.bioskopbackend.entities.Ticket;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface TicketRepository extends CrudRepository<Ticket, Integer> {
    List<Ticket> findAll();

    @Query(value = "SELECT * FROM ticket WHERE id_schedule = :idJadwal", nativeQuery = true)
    List<Ticket> findByJadwal(@Param("idJadwal") int idJadwal);

    @Query(value = "SELECT new id.co.nexsoft.bioskopbackend.entities.Ticket" +
            "(t.id, t.schedule, t.seat, t.status) FROM Ticket t " +
            "JOIN OrderanDetail od " +
            "ON od.ticket.id = t.id " +
            "JOIN Orderan o " +
            "ON o.id = od.orderan.id " +
            "WHERE o.id = :idOrderan")
    List<Ticket> findTicketByOrderanId(@Param("idOrderan") int idOrderan);

    @Query(value ="SELECT COUNT(t.status) " +
            "FROM ticket t JOIN schedule s " +
            "ON s.id = t.id_schedule " +
            "WHERE s.show_date = :today " +
            "AND t.status = :status", nativeQuery = true)
    int findTicketSoldoutToday(@Param("today") LocalDate today,@Param("status") String status);

    @Query(value = "SELECT COUNT(r.price) " +
            "FROM ticket t " +
            "JOIN schedule s " +
            "ON s.id = t.id_schedule " +
            "JOIN room r " +
            "ON s.id_room = r.id " +
            "WHERE s.show_date = :today " +
            "AND t.status = :status", nativeQuery = true)
    int findCountIncomesToday(@Param("today") LocalDate today,@Param("status") String status);

    @Query(value = "SELECT SUM(r.price) " +
            "FROM ticket t " +
            "JOIN schedule s " +
            "ON s.id = t.id_schedule " +
            "JOIN room r " +
            "ON s.id_room = r.id " +
            "WHERE s.show_date = :today " +
            "AND t.status = :status", nativeQuery = true)
    int findIncomesToday(@Param("today") LocalDate today,@Param("status") String status);

    @Modifying
    @Transactional
    @Query(value = "INSERT INTO ticket(id_seat, id_schedule, status) " +
            "VALUES(:#{#ticket.idSeat}, :#{#ticket.idSchedule}, :#{#ticket.status})" , nativeQuery = true)
    void checkoutTicket(@Param("ticket") TicketRequest ticket);

    // @Query(value = "SELECT * FROM ticket ORDER BY id DESC limit 1", nativeQuery = true)
    Ticket findTopByOrderByIdDesc();

    @Modifying
    @Transactional
    @Query(value = "UPDATE orderan o " +
            "JOIN orderan_detail od " +
            "ON o.id = od.orderan_id " +
            "JOIN ticket t " +
            "ON t.id = od.id_ticket " +
            "SET t.status = :status " +
            "WHERE o.id = :id", nativeQuery = true)
    void reserveTicked(@Param("id") int id, @Param("status") String status);

    @Query(value = "SELECT COUNT(id) " +
            "FROM ticket " +
            "WHERE id_seat = :seatId " +
            "AND id_schedule = :id ", nativeQuery = true)
    int findTicketReserved(@Param("seatId") String seatId,@Param("id") int idSchedule);

    @Transactional
    @Modifying
    @Query( value="DELETE FROM ticket WHERE id = :id", nativeQuery = true)
    void rejectTicket(@Param("id") int id);

}
