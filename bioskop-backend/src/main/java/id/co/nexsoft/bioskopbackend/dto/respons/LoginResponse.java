package id.co.nexsoft.bioskopbackend.dto.respons;

import id.co.nexsoft.bioskopbackend.dto.requests.UserDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

public class LoginResponse {

    private UserDTO user;

    private String role;

    private String token;
}
