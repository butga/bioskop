package id.co.nexsoft.bioskopbackend.services;

import id.co.nexsoft.bioskopbackend.dto.requests.OrderanDetailRequest;
import id.co.nexsoft.bioskopbackend.dto.requests.OrderanRequest;
import id.co.nexsoft.bioskopbackend.dto.requests.TicketRequest;
import id.co.nexsoft.bioskopbackend.dto.respons.MemberOrderResponse;
import id.co.nexsoft.bioskopbackend.dto.respons.OrderanDetailResponse;
import id.co.nexsoft.bioskopbackend.entities.Orderan;
import id.co.nexsoft.bioskopbackend.entities.Schedule;
import id.co.nexsoft.bioskopbackend.entities.Ticket;
import id.co.nexsoft.bioskopbackend.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Locale;

@Service
public class OrderanService {

    @Autowired
    private OrderanRepository orderanRepository;

    @Autowired
    private TicketRepository ticketRepository;

    @Autowired
    private ScheduleRepository scheduleRepository;

    @Autowired
    private OrderanDetailRepository orderanDetailRepository;

    @Autowired
    private EmailService emailService;

    @Autowired
    private UserRepository userRepository;

    @Transactional
    public ResponseEntity<?> checkoutOrderan(OrderanRequest or) {

        // backend validasi kursi
        for (String idSeat : or.getListSeat()) {
            if (ticketRepository.findTicketReserved(idSeat, or.getIdSchedule()) != 0) {
                return ResponseEntity.badRequest()
                        .body("This seat has been reserved");
            }
        }

        // cek apakah jadwal masih ready
        Schedule schedule = scheduleRepository.findById(or.getIdSchedule());

        if( LocalDate.now().isAfter(schedule.getShowDate())) {
            return ResponseEntity.badRequest()
                    .body("This Schedule Date is expired");
        } else if ( LocalTime.now().isAfter(schedule.getShowTime().plusMinutes(30)) && LocalDate.now().isEqual(schedule.getShowDate())) {
            return ResponseEntity.badRequest()
                    .body("This Time is over limit");
        }

        // buat orderan
        or.setOrderDate(LocalDate.now());
        or.setOrderStatus("requested");
        orderanRepository.checkoutOrderan(or);
        Orderan orderan = orderanRepository.findTopByOrderByIdDesc();

        // buat ticket
        for( String seat : or.getListSeat() ) {
            TicketRequest ticketRequest = new TicketRequest();
            ticketRequest.setIdSchedule(or.getIdSchedule());
            ticketRequest.setIdSeat(seat);
            ticketRequest.setStatus("requested");
            ticketRepository.checkoutTicket(ticketRequest);

            Ticket ticket = ticketRepository.findTopByOrderByIdDesc();

            // buat orderan detail
            OrderanDetailRequest odr = new OrderanDetailRequest();
            odr.setIdOrder(orderan.getId());
            odr.setIdTicket(ticket.getId());
            orderanDetailRepository.checkoutOD(odr);
        }

        // Order Detail Email Service
        OrderanDetailResponse odr = orderanRepository.findOrderanDetail(orderan.getId());

        // cetak kode kursi
        StringBuilder listSeatBuilder = new StringBuilder();
        for(String seat : orderanRepository.findSeatOrderDetail(orderan.getId())) {
            listSeatBuilder.append("[ " + seat + " ] ");
        }

        String listSeat = listSeatBuilder.toString();

        // hitung harga ticket
        int totalPrice = orderanRepository.findTotalPriceOrder(orderan.getId());

        // format rupiah
        Locale myIndonesianLocale = new Locale("in", "ID");
        NumberFormat formater = NumberFormat.getCurrencyInstance(myIndonesianLocale);
        String totalPriceFormatted = formater.format(totalPrice);

        odr.setTicket(orderanRepository.findSeatOrderDetail(orderan.getId()).size());
        odr.setSeats(listSeat);

        String emailUser = userRepository.findEmailById(orderan.getUser().getId());
        String topic = "CHECKOUT ORDER #"+orderan.getId();
        String body = "Terimakasih Atas Pemesanan Anda! " +
                "Nomor order anda adalah #" + orderan.getId() + " " +
                "Berikut detail order Anda.\n"+
                "Nama: "+odr.getName()+"\n" +
                "Email: "+odr.getEmail()+"\n" +
                "Phone: "+odr.getPhone_number()+"\n" +
                "Jumlah Ticket: "+ odr.getTicket()+"\n" +
                "Kode Kursi: "+ listSeat +" \n" +
                "Total Price: " + totalPriceFormatted + "\n" +
                "Tanggal Order: "+ odr.getOrderDate() +"\n\n" +
                "Silahkan lakukan pembayaran ke rekening berikut. \n" +
                "Nama Pemilik: NEXMOVI \n" +
                "Nomor Rekening: 8857 3265 6686 \n" +
                "Nama Bank: BCA \n\n" +
                "Setelah transfer, reply email ini beserta bukti transfer.\n" +
                "Anda akan menerima email pemberitahuan setelah admin mengkonfirmasi pembayaran Anda. Terimakasih!";

        emailService.sendEmail(emailUser, body, topic);

        return ResponseEntity.ok()
                .body(or);
    }

    public ResponseEntity<?> findOrderanPaginated(int pageNo, int pageSize) {

        Pageable pageable = PageRequest.of(pageNo, pageSize);

        String status = "requested";

        Page<MemberOrderResponse> orders = orderanRepository.findOrderanPaginated(pageable, status);

        return ResponseEntity.ok()
                .body(orders);
    }

    public ResponseEntity<?> findOrderanAccepted(int pageNo, int pageSize) {

        Pageable pageable = PageRequest.of(pageNo, pageSize);

        String status = "accepted";

        Page<MemberOrderResponse> orders = orderanRepository.findOrderanPaginated(pageable, status);

        return ResponseEntity.ok()
                .body(orders);
    }

    public ResponseEntity<?> findMyOrders(int pageNo, int pageSize, int id) {

        Pageable pageable = PageRequest.of(pageNo, pageSize);

        Page<MemberOrderResponse> orders = orderanRepository.findMyOrders(pageable, id);

        return ResponseEntity.ok()
                .body(orders);
    }

    @Transactional
    public ResponseEntity<?> confirmOrderan(int id) {

        String orderStatus = "accepted";
        String ticketStatus = "reserved";

        orderanRepository.confirmOrderan(id, orderStatus);
        ticketRepository.reserveTicked(id, ticketStatus);

        Orderan updatedOrderan = orderanRepository.findById(id);

        // Detail order
        OrderanDetailResponse odr = orderanRepository.findOrderanDetail(id);

        StringBuilder listSeatBuilder = new StringBuilder();
        for(String seat : orderanRepository.findSeatOrderDetail(id)) {
            listSeatBuilder.append("[ " + seat + " ] ");
        }

        String listSeat = listSeatBuilder.toString();

        // hitung harga ticket
        int totalPrice = orderanRepository.findTotalPriceOrder(id);

        // format rupiah
        Locale myIndonesianLocale = new Locale("in", "ID");
        NumberFormat formater = NumberFormat.getCurrencyInstance(myIndonesianLocale);
        String totalPriceFormatted = formater.format(totalPrice);

        odr.setTicket(orderanRepository.findSeatOrderDetail(id).size());
        odr.setSeats(listSeat);

        String emailUser = userRepository.findEmailById(updatedOrderan.getUser().getId());
        String topic = "RESERVED ORDER #"+id ;
        String body = "Selamat, pembayaran anda telah dikonfirmasi. " +
                "Berikut detail order anda. \n" +
                "Nama: "+odr.getName()+"\n" +
                "Email: "+odr.getEmail()+"\n" +
                "Phone: "+odr.getPhone_number()+"\n" +
                "Jumlah Ticket: "+ odr.getTicket()+"\n" +
                "Kode Kursi: "+ listSeat +" \n" +
                "Total Price: " + totalPriceFormatted + "\n" +
                "Tanggal Order: "+ odr.getOrderDate() +"\n\n" +
                "Terimakasih. Selamat menonton!";

        emailService.sendEmail(emailUser, body, topic);

        return ResponseEntity.ok()
                .body(updatedOrderan);
    }

    @Transactional
    public ResponseEntity<?> rejectOrderan(int id, int pageNo, int pageSize) {

        if (orderanRepository.findById(id) == null) {
            return ResponseEntity.badRequest()
                    .body("order not found");
        }

        List<Integer> listIdTicket = orderanDetailRepository.findIdTicketByOrderId(id);

        orderanDetailRepository.deleteOrderanDetail(id);
        orderanRepository.deleteOrderan(id);
        for (Integer idTicket : listIdTicket) {
            ticketRepository.rejectTicket(idTicket);
        }

        Pageable pageable = PageRequest.of(pageNo, pageSize);

        String status = "requested";

        Page<MemberOrderResponse> orders = orderanRepository.findOrderanPaginated(pageable, status);

        return ResponseEntity.ok()
                .body(orders);
    }
}
