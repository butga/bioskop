package id.co.nexsoft.bioskopbackend.services;

import id.co.nexsoft.bioskopbackend.dto.requests.TicketRequest;
import id.co.nexsoft.bioskopbackend.entities.Ticket;
import id.co.nexsoft.bioskopbackend.repositories.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TicketService {

    @Autowired
    TicketRepository ticketRepository;

    public ResponseEntity<?> findAllTicket() {
        List<Ticket> tickets = ticketRepository.findAll();
        return ResponseEntity.ok()
                .body(tickets);
    }

    public ResponseEntity<?> findTicketByJadwal(int idJadwal) {
        List<Ticket> tickets = ticketRepository.findByJadwal(idJadwal);
        return ResponseEntity.ok()
                .body(tickets);
    }

    public ResponseEntity<?> findTicketByOrderanId(int idOrderan) {
        List<Ticket> ticketsMember = ticketRepository.findTicketByOrderanId(idOrderan);
        return ResponseEntity.ok()
                .body(ticketsMember);
    }

}
