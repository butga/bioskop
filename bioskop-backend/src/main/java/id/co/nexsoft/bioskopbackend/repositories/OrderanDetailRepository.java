package id.co.nexsoft.bioskopbackend.repositories;

import id.co.nexsoft.bioskopbackend.dto.requests.OrderanDetailRequest;
import id.co.nexsoft.bioskopbackend.entities.OrderanDetail;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface OrderanDetailRepository extends CrudRepository<OrderanDetail, Integer> {

    @Modifying
    @Transactional
    @Query(value = "INSERT INTO orderan_detail(orderan_id, id_ticket) " +
            "VALUES(:#{#odr.idOrder}, :#{#odr.idTicket})", nativeQuery = true)
    void checkoutOD(@Param("odr") OrderanDetailRequest odr);

    @Transactional
    @Modifying
    @Query( value="DELETE FROM orderan_detail WHERE orderan_id = :id", nativeQuery = true)
    void deleteOrderanDetail(@Param("id") int id);

    @Query( value = "SELECT id_ticket FROM orderan_detail WHERE orderan_id = :id", nativeQuery = true)
    List<Integer> findIdTicketByOrderId(int id);
}
