package id.co.nexsoft.bioskopbackend.services;

import id.co.nexsoft.bioskopbackend.entities.Seat;
import id.co.nexsoft.bioskopbackend.repositories.SeatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SeatService {

    @Autowired
    private SeatRepository seatRepository;

    public ResponseEntity<?> addSeat(List<Seat> seats) {

        seatRepository.saveAll(seats);

        List<Seat> listSeat = seatRepository.findAll();
        return ResponseEntity.ok()
                .body(listSeat);
    }

    public ResponseEntity<?> findAllSeat() {
        List<Seat> listSeat = seatRepository.findAll();
        return ResponseEntity.ok()
                .body(listSeat);
    }

    public ResponseEntity<?> generateDummySeat() {

        List<Seat> seats = new ArrayList<Seat>();

        for(int i = 1; i <= 6; i++ ) {
            Seat seatLoop = new Seat();
            seatLoop.setId("U" + i);
            seatLoop.setName("U" + i);

            seats.add(seatLoop);
        }

        for(int i = 1; i <= 6; i++ ) {
            Seat seatLoop = new Seat();
            seatLoop.setId("D" + i);
            seatLoop.setName("D" + i);

            seats.add(seatLoop);
        }

        for(int i = 1; i <= 25; i++ ) {
            Seat seatLoop = new Seat();
            seatLoop.setId("L" + i);
            seatLoop.setName("L" + i);

            seats.add(seatLoop);
        }

        for(int i = 1; i <= 25; i++ ) {
            Seat seatLoop = new Seat();
            seatLoop.setId("R" + i);
            seatLoop.setName("R" + i);

            seats.add(seatLoop);
        }

        seatRepository.saveAll(seats);

        List<Seat> listSeat = seatRepository.findAll();
        return ResponseEntity.ok()
                .body(listSeat);
    }
}
