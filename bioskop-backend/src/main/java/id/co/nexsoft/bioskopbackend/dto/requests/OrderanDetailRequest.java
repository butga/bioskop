package id.co.nexsoft.bioskopbackend.dto.requests;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

public class OrderanDetailRequest {

    private int idOrder;

    private int idTicket;

}
