package id.co.nexsoft.bioskopbackend.services;

import id.co.nexsoft.bioskopbackend.dto.respons.DashboardResponse;
import id.co.nexsoft.bioskopbackend.repositories.ScheduleRepository;
import id.co.nexsoft.bioskopbackend.repositories.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class DashboardService {

    @Autowired
    private ScheduleRepository scheduleRepository;

    @Autowired
    private TicketRepository ticketRepository;

    public ResponseEntity<?> getData() {

        LocalDate today = LocalDate.now();
        DashboardResponse dataDashboard = new DashboardResponse();

        int moviesRepo = scheduleRepository.findMoviesToday(today);
        if (moviesRepo == 0) dataDashboard.setMovies(0);
        else dataDashboard.setMovies(moviesRepo);

        int ticketsRepo = ticketRepository.findTicketSoldoutToday(today, "reserved");
        if (ticketsRepo == 0) dataDashboard.setTickets(0);
        else dataDashboard.setTickets(ticketsRepo);

        int countIncomesRepo = ticketRepository.findCountIncomesToday(today, "reserved");
        if (countIncomesRepo == 0) dataDashboard.setIncomes(0);
        else {
            int incomesRepo = ticketRepository.findIncomesToday(today, "reserved");
            dataDashboard.setIncomes(incomesRepo);
        }

        return ResponseEntity.ok()
                .body(dataDashboard);
    }
}
