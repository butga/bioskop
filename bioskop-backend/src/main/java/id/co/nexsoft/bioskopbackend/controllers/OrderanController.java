package id.co.nexsoft.bioskopbackend.controllers;

import id.co.nexsoft.bioskopbackend.dto.requests.OrderanRequest;
import id.co.nexsoft.bioskopbackend.services.OrderanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
public class OrderanController {

    @Autowired
    private OrderanService orderanService;

    @PostMapping("/admin/orderan/checkout")
    public ResponseEntity<?> checkoutOrderan(@Valid @RequestBody OrderanRequest or) {
        return orderanService.checkoutOrderan(or);
    }

    @GetMapping("/admin/orderan")
    public ResponseEntity<?> findOrderanPaginated(
            @RequestParam(defaultValue = "0") int pageNo,
            @RequestParam(defaultValue = "5") int pageSize
    ) {
        return orderanService.findOrderanPaginated(pageNo, pageSize);
    }

    @GetMapping("/admin/orderan-accepted")
    public ResponseEntity<?> findOrderanAccepted(
            @RequestParam(defaultValue = "0") int pageNo,
            @RequestParam(defaultValue = "5") int pageSize
    ) {
        return orderanService.findOrderanAccepted(pageNo, pageSize);
    }

    @GetMapping("/member/orderans")
    public ResponseEntity<?> findMyOrders(
            @RequestParam(defaultValue = "0") int pageNo,
            @RequestParam(defaultValue = "5") int pageSize,
            @RequestParam int id
    ) {
        return orderanService.findMyOrders(pageNo, pageSize, id);
    }

    @PostMapping("admin/confirm-orderan")
    public ResponseEntity<?> confirmOrderan(@RequestParam int id)  {
        return orderanService.confirmOrderan(id);
    }

    @PostMapping("admin/reject-orderan")
    public ResponseEntity<?> rejectOrderan(
            @RequestParam int id,
            @RequestParam(defaultValue = "0") int pageNo,
            @RequestParam(defaultValue = "5") int pageSize
    )  {
        return orderanService.rejectOrderan(id, pageNo, pageSize);
    }
}
