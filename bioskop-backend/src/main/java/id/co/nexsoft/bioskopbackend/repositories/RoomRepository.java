package id.co.nexsoft.bioskopbackend.repositories;

import id.co.nexsoft.bioskopbackend.entities.Room;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface RoomRepository extends CrudRepository<Room, Integer> {

    @Query(value = "SELECT " +
            "id, name, type, price " +
            "FROM room r " +
            "WHERE r.id = :id", nativeQuery = true)
    Room findRoom(@Param("id") int id);

    @Query(value = "SELECT " +
            "id, name, type, price " +
            "FROM room  ", nativeQuery = true)
    List<Room> findAllRoom();

    @Query(value = "SELECT " +
            "id, name, type, price " +
            "FROM room " +
            "ORDER BY room.id DESC", nativeQuery = true)
    Page<Room> findRoomPaginated(Pageable pageable);

    @Modifying
    @Transactional
    @Query(value = "INSERT INTO room(id, name, type, price) " +
            "VALUES(:#{#room.id}, :#{#room.name}, :#{#room.type}, :#{#room.price})"
            , nativeQuery = true)
    void saveRoom(@Param("room") Room room);

    @Modifying
    @Transactional
    @Query(value ="UPDATE room r " +
            "SET r.name = :#{#room.name}, " +
            "r.type = :#{#room.type}, " +
            "r.price = :#{#room.price} " +
            "WHERE r.id = :id", nativeQuery = true)
    void updateRoom(@Param("id") int id, @Param("room") Room room);

    Room findByName(String name);
}
