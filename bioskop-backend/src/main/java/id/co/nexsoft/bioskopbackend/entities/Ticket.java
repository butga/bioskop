package id.co.nexsoft.bioskopbackend.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

@Entity
public class Ticket {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private int id;

    @ManyToOne
    @JoinColumn(name = "id_schedule")
    private Schedule schedule;

    @OneToOne
    @JoinColumn(name= "id_seat")
    private Seat seat;

    @NotBlank
    private String status;

}
