package id.co.nexsoft.bioskopbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BioskopBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(BioskopBackendApplication.class, args);
    }

}
