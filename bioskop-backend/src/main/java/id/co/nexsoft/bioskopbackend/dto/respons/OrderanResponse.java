package id.co.nexsoft.bioskopbackend.dto.respons;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class OrderanResponse {

    private int id;

    private String username;

    private LocalDate orderDate;

    private String orderStatus;

    private String name;

    private String email;

    private String movieName;

}
