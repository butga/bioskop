package id.co.nexsoft.bioskopbackend.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

@Entity
public class OrderanDetail {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private int id;

    @ManyToOne(targetEntity = Orderan.class)
    @JsonIgnore
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Orderan orderan;

    @ManyToOne
    @JoinColumn(name = "id_ticket")
    private Ticket ticket;

}
